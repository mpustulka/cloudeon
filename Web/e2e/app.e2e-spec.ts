import { CmdbAppPage } from './app.po';

describe('cmdb-app App', () => {
  let page: CmdbAppPage;

  beforeEach(() => {
    page = new CmdbAppPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
