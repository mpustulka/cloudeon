import {  RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    { path: 'ci', title: 'Configuration items', icon: 'widgets', class: '' },
    { path: 'customers', title: 'Customers',  icon:'people', class: '' },
    { path: 'settings', title: 'Settings',  icon:'settings', class: '' }
];
