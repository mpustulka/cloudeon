﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { Tenant } from './../../services/model/tenant';
import { CustomerService } from './../../services/customer.service';

@Component({
    selector: 'tenants-cmp',
    templateUrl: 'tenants.component.html'
})


export class TenantsComponent implements OnInit{
  public view: any;
  public id: string;
  private editDataItem: Tenant;

  constructor(private service: CustomerService, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
      this.id = this.route.snapshot.params['id'];
      this.service.getTenant(this.id).then(res => this.view = res);
  }

  public editHandler({ dataItem }) {
    this.editDataItem = dataItem;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
  }

  public saveHandler(tenant: Tenant) {
    this.service.updateTenant(tenant);

    this.editDataItem = undefined;
  }

  public removeHandler({ dataItem }) {
    this.service.removeTenant(dataItem);
  }
}