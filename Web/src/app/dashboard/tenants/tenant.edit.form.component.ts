﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Tenant } from './../../services/model/tenant';

@Component({
  selector: 'kendo-grid-edit-form',
  styles: [
    "input[type=text] { width: 100%; }"
  ],
  template: `
        <kendo-dialog *ngIf="active" (close)="closeForm()">
          <kendo-dialog-titlebar>
            {{ isNew ? 'Add new tenant' : 'Edit tenant' }}
          </kendo-dialog-titlebar>

            <form novalidate [formGroup]="editForm">
                <div class="form-group">
                    <label for="name" class="control-label">Name</label>

                    <input type="text" class="k-textbox" formControlName="name" />

                    <div class="k-tooltip k-tooltip-validation" [hidden]="editForm.controls.name.valid || editForm.controls.name.pristine">
                        Tenant Name is required
                    </div>
                </div> 
              <div class="form-group">
                    <label for="tenantid" class="control-label">Tenant ID</label>

                    <input type="text" class="k-textbox" formControlName="tenantid" />

                    <div class="k-tooltip k-tooltip-validation" [hidden]="editForm.controls.tenantid.valid || editForm.controls.tenantid.pristine">
                        Tenant ID is required
                    </div>
                </div>
                <div class="form-group">
                    <label>
                      <input type="checkbox" formControlName="_syncfresh" />
                      Sync to Freshservice
                    </label>
                </div>
            </form>

            <kendo-dialog-actions>
                <button class="k-button" (click)="onCancel($event)">Cancel</button>
                <button class="k-button k-primary" [disabled]="!editForm.valid" (click)="onSave($event)">Save</button>
            </kendo-dialog-actions>
        </kendo-dialog>
    `
})
export class GridEditFormComponent {
  private editForm = new FormGroup({
    'tenantid': new FormControl("", Validators.required),
    'name': new FormControl("", Validators.required),
    '_syncfresh': new FormControl(false)
  });

  private active: boolean = false;
  @Input() public isNew: boolean = false;

  @Input() public set model(tenant: Tenant) {
    this.editForm.reset(tenant);

    this.active = tenant !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<Tenant> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
