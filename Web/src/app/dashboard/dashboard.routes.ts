﻿import { Route } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomerDetailsComponent } from './customers/customer.details.component';
import { TenantsComponent } from './tenants/tenants.component';
import { ConfigurationItemsComponent } from './ci/ci.list.component';
import { ConfigurationItemComponent } from './ci/ci.details.component';
import { SettingsComponent } from './settings/settings.component';

export const MODULE_ROUTES: Route[] = [
  { path: 'customers', component: CustomersComponent },
  { path: 'customer/:id', component: CustomerDetailsComponent },
  { path: 'tenant/:id', component: TenantsComponent },
  { path: 'ci', component: ConfigurationItemsComponent },
  { path: 'ci/:id', component: ConfigurationItemComponent },
  { path: 'settings', component: SettingsComponent },
  { path: '', redirectTo: 'customers', pathMatch: 'full' }
];

export const MODULE_COMPONENTS = [
  CustomersComponent,
  ConfigurationItemsComponent,
  SettingsComponent,
  CustomerDetailsComponent,
  TenantsComponent,
  ConfigurationItemComponent
];