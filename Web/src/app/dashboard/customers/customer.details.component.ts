﻿import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { CustomerService } from './../../services/customer.service';

@Component({
    selector: 'customer-details-cmp',
    templateUrl: 'customer.details.component.html'
})


export class CustomerDetailsComponent implements OnInit{
  public customer: any;
  public id: string;
  private checkedTypes: any[] = [];

  constructor(private service: CustomerService, private route: ActivatedRoute) {
    
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.service.getCustomer(this.id).then(res => this.customer = res);
  }

  onSubmit(f: NgForm) {
    console.log(f.value);  // { first: '', last: '' }
    console.log(f.valid);  // false
  }

  private logCheckbox(element: HTMLInputElement): void {
    if (element.checked)
      this.checkedTypes.push(element.value);
  }
}