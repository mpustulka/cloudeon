﻿import { Component } from '@angular/core';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { CustomerService } from './../../services/customer.service';

@Component({
    selector: 'customers-cmp',
    templateUrl: 'customers.component.html'
})


export class CustomersComponent{
  public view: any[];

  constructor(private service: CustomerService) {
    this.service.getCustomers().subscribe(r => {
      this.view = r.data;
    });
  }
}