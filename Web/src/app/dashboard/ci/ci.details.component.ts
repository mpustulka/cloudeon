﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

import { CIService } from './../../services/ci.service';

@Component({
  selector: 'configuration-items-details-cmp',
  templateUrl: 'ci.details.component.html'
})
export class ConfigurationItemComponent implements OnInit {
  lifeCycles: any[] = [
    "planned",
    "ordered",
    "installed",
    "in test",
    "in production",
    "retired"
  ];
  public ci: any;
  public id: string;
  public keys: any[];

  constructor(private service: CIService, private route: ActivatedRoute, private location: Location) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.service.getCI(this.id).then(res => this.createObjectKeysList(res));
  }


  onSubmit(f: NgForm) {
    console.log(f);
    //var now = new Date();
    //var utc = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
    //model._syncfresh = false;
    //model._syncfreshreason = value;
    //model._syncfreshrejectedby = 'Test';
    //model._syncfreshdate = utc;
  }

  private createObjectKeysList(ci: any) {
    let keys = Object.keys(ci);
    if (keys.indexOf('_rid') >= 0) keys.splice(keys.indexOf('_rid'), 1);
    if (keys.indexOf('_lifecycle') >= 0) keys.splice(keys.indexOf('_lifecycle'), 1);
    if (keys.indexOf('_self') >= 0) keys.splice(keys.indexOf('_self'), 1);
    if (keys.indexOf('_etag') >= 0) keys.splice(keys.indexOf('_etag'), 1);
    if (keys.indexOf('_attachments') >= 0) keys.splice(keys.indexOf('_attachments'), 1);
    if (keys.indexOf('_modified') >= 0) keys.splice(keys.indexOf('_modified'), 1);
    if (keys.indexOf('_tag') >= 0) keys.splice(keys.indexOf('_tag'), 1);


    if (keys.indexOf('_parent') >= 0) keys.splice(keys.indexOf('_parent'), 1);
    if (keys.indexOf('_type') >= 0) keys.splice(keys.indexOf('_type'), 1);
    if (keys.indexOf('_syncfresh') >= 0) keys.splice(keys.indexOf('_syncfresh'), 1);


    if (keys.indexOf('$promise') >= 0) keys.splice(keys.indexOf('$promise'), 1);
    if (keys.indexOf('$resolved') >= 0) keys.splice(keys.indexOf('$resolved'), 1);
    this.keys = keys;
    this.ci = ci;
  }

  public declineCI() {
    this.service.changeCIState(this.id, false);
    window.location.reload();
  }

  public back() {
    this.location.back();
  }
}