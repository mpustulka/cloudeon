﻿import { Component } from '@angular/core';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { CIService } from './../../services/ci.service';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';

@Component({
  selector: 'configuration-items-cmp',
  templateUrl: 'ci.list.component.html'
})
export class ConfigurationItemsComponent {
  public discoveredCIList: any[];
  public ciList: any[];
  public declinedCIList: any[];
  private selectedItems: any[] = [];
  private state: State = {};

  constructor(private service: CIService) {
    this.service.getDiscoveredCIList().subscribe(r => {
      for (var i = 0; i < r.total; i++) {
        r.data[i]._created = new Date(r.data[i]._created);
      }
      this.discoveredCIList = r.data;
    });
    this.service.getCIList().subscribe(c => {
      this.ciList = c.data;
    });

    this.service.getDeclinedCIList().subscribe(c => {
      this.declinedCIList = c.data;
    });
  }

  protected dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    process(this.discoveredCIList, this.state);
  }

  public showDeclineReason(dataItem: any, index: number): boolean {
    return dataItem._syncfreshreason != null;
  }

  public changeCIState(e: any, id: string, state: boolean): void {
    var idx = this.selectedItems.findIndex(item => item.id === id);
    if (idx >= 0)
      this.selectedItems[idx] = { id: id, state: state };
    else
      this.selectedItems.push({ id: id, state: state });
  }

  public declineCI(id: string) {
    this.service.changeCIState(id, false);
    window.location.reload();
  }

  public acceptCI(id: string) {
    this.service.changeCIState(id, true);
    window.location.reload();
  }

  public bulkChangeCI() {
    this.service.bulkChangeCIState(this.selectedItems);
    window.location.reload();
  }
}