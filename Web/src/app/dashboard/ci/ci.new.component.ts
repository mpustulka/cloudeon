﻿import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { CIService } from './../../services/ci.service';
import { CustomerService } from './../../services/customer.service';
import { CI } from './model/ci';

@Component({
    selector: 'configuration-items-new-cmp',
    templateUrl: 'ci.new.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class NewConfigurationItemsComponent implements OnInit {
  uploadSaveUrl: string;
  customers: any[] = [];
  types: any[] = [];
  customProperties: any[] = [];
  @Input() public newCI : CI = {};
  public opened: boolean = false;
  public dialogText: string;
  saving: boolean = false;

  constructor(private ciService: CIService, private customerService: CustomerService) {
  }
  
  ngOnInit(): void {
    this.uploadSaveUrl = this.ciService.getCIExcelURL();
    this.customerService.getCustomers().subscribe(r => {
      this.customers = r.data;
    });

    this.ciService.getCITypesList().subscribe(r => {
      this.types = r.data;
    });
  }

  public onClose() {
    this.opened = false;
    if (this.saving)
      window.location.reload();
  }

  public removeProperty(idx: number) {
    this.customProperties.splice(idx, 1);
  }

  public addProperty() {
    this.customProperties.push({ name: '', value: '' });
  }

  private openDialog(success: boolean) {
    if (!success) {
      this.dialogText = "Customer and CI type are mandatory!";
    } else {
      this.dialogText = "New CI has been created and is marked as discovered.";
    }
    this.opened = true;
  }

  public saveCI(): void {
    if (this.saving) return;

    let ci: CI = {};
    if (this.newCI.type == undefined || this.newCI.type.length == 0) {
      this.openDialog(false);
      return;
    }

    ci.type = this.newCI.type;

    if (this.newCI.customer != undefined) {
      ci.customername = this.newCI.customer.name;
      ci.customerid = this.newCI.customer.id;
    } else {
      this.openDialog(false);
      return;
    }

    for (let i of this.customProperties) {
      ci[i.name] = i.value;
    }

    this.saving = true;
    this.ciService.create(ci).then(function (res) {
      window.location.reload();
    }.bind(this));
  }
}