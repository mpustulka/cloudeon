﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MODULE_COMPONENTS, MODULE_ROUTES } from './dashboard.routes';

import { GridModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { UploadModule } from '@progress/kendo-angular-upload';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { NewConfigurationItemsComponent } from './ci/ci.new.component';
import { GridEditFormComponent } from './tenants/tenant.edit.form.component';

import { CustomerService } from './../services/customer.service';
import { CIService } from './../services/ci.service';

@NgModule({
  imports: [
    HttpModule,
    FormsModule,
    GridModule,
    DropDownsModule,
    DialogModule,
    UploadModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(MODULE_ROUTES)
  ],
  providers: [CustomerService, CIService],
  declarations: [
    MODULE_COMPONENTS,
    NewConfigurationItemsComponent,
    GridEditFormComponent
  ]
})

export class DashboardModule { }