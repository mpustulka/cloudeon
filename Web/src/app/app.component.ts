﻿import { Component, ViewEncapsulation, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
      './app.component.scss',
      '../../node_modules/@progress/kendo-theme-default/dist/all.css',
      '../../node_modules/bootstrap/dist/css/bootstrap.min.css',
      '../assets/css/material-dashboard.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    ngOnInit() {
        $.getScript('assets/js/material-dashboard.js');
        $.getScript('assets/js/initMenu.js');
    }
}