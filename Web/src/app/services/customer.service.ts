﻿import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { GridDataResult } from '@progress/kendo-angular-grid';

import 'rxjs/add/operator/toPromise';

import { CMDBService} from './cmdb.service';
import { Customer } from './model/customer';
import { Tenant } from './model/tenant';


@Injectable()
export class CustomerService extends CMDBService{

  private headers = new Headers({ 'Authorization' : `Bearer &lt;saml:Assertion MajorVersion=&quot;1&quot; MinorVersion=&quot;1&quot; AssertionID=&quot;_fe2db076-75d5-40a5-b326-6a2438b735de&quot; Issuer=&quot;urn:cmdb-sts-prod.azurewebsites.net&quot; IssueInstant=&quot;2017-05-31T11:36:11.580Z&quot; xmlns:saml=&quot;urn:oasis:names:tc:SAML:1.0:assertion&quot;&gt;&lt;saml:Conditions NotBefore=&quot;2017-05-31T11:36:11.580Z&quot; NotOnOrAfter=&quot;2017-05-31T21:36:11.580Z&quot;&gt;&lt;saml:AudienceRestrictionCondition&gt;&lt;saml:Audience&gt;https://cmdb-api-data-prod.azurewebsites.net/&lt;/saml:Audience&gt;&lt;/saml:AudienceRestrictionCondition&gt;&lt;/saml:Conditions&gt;&lt;saml:AttributeStatement&gt;&lt;saml:Subject&gt;&lt;saml:NameIdentifier&gt;7909c828-6068-48ba-bf38-0494f3a950ce&lt;/saml:NameIdentifier&gt;&lt;saml:SubjectConfirmation&gt;&lt;saml:ConfirmationMethod&gt;urn:oasis:names:tc:SAML:1.0:cm:bearer&lt;/saml:ConfirmationMethod&gt;&lt;/saml:SubjectConfirmation&gt;&lt;/saml:Subject&gt;&lt;saml:Attribute AttributeName=&quot;name&quot; AttributeNamespace=&quot;http://schemas.xmlsoap.org/ws/2005/05/identity/claims&quot;&gt;&lt;saml:AttributeValue&gt;Grzegorz Grochowski&lt;/saml:AttributeValue&gt;&lt;/saml:Attribute&gt;&lt;saml:Attribute AttributeName=&quot;upn&quot; AttributeNamespace=&quot;http://schemas.xmlsoap.org/ws/2005/05/identity/claims&quot;&gt;&lt;saml:AttributeValue&gt;grg@cloudeon.com&lt;/saml:AttributeValue&gt;&lt;/saml:Attribute&gt;&lt;/saml:AttributeStatement&gt;&lt;Signature xmlns=&quot;http://www.w3.org/2000/09/xmldsig#&quot;&gt;&lt;SignedInfo&gt;&lt;CanonicalizationMethod Algorithm=&quot;http://www.w3.org/2001/10/xml-exc-c14n#&quot;/&gt;&lt;SignatureMethod Algorithm=&quot;http://www.w3.org/2001/04/xmldsig-more#rsa-sha256&quot;/&gt;&lt;Reference URI=&quot;#_fe2db076-75d5-40a5-b326-6a2438b735de&quot;&gt;&lt;Transforms&gt;&lt;Transform Algorithm=&quot;http://www.w3.org/2000/09/xmldsig#enveloped-signature&quot;/&gt;&lt;Transform Algorithm=&quot;http://www.w3.org/2001/10/xml-exc-c14n#&quot;/&gt;&lt;/Transforms&gt;&lt;DigestMethod Algorithm=&quot;http://www.w3.org/2001/04/xmlenc#sha256&quot;/&gt;&lt;DigestValue&gt;/YXHC4klSFPrxvBeO8RSVi5J6uc2JlLy3WIsPGCuJMU=&lt;/DigestValue&gt;&lt;/Reference&gt;&lt;/SignedInfo&gt;&lt;SignatureValue&gt;Xn1OHgEagLa90k+Bkgshrn/cz6rSlALtMQmL/LIP6vH6Vx39qwJmUdSb+BPxt7005BCeipLSXb/BSNh0yvfT0yRYuBN0RV7ja9tTYQe5L+f60emVUrzEkNcjQAReetDHTiEXd9HtMrmlc6WKlpg/w+zhc/mxae2nNITTXDXtl7xsk5mW4qnwdn6dUc1ousvsakCNIWy4SIz+XcV8yFSJ2MHmlWjZHiRsfRngnPc9BtLfLO14IgsyteAQ8pivtMkKW2dsUjuyM5KsalAmhOF4/rjEDvezAUHmZkp9/Pe28tB0mos/NHYEnKU0/8ZUT7iG/CHFwmjsB2Zz99ng9Rnx+g==&lt;/SignatureValue&gt;&lt;KeyInfo&gt;&lt;X509Data&gt;&lt;X509Certificate&gt;MIIFMjCCBBqgAwIBAgIIdXc26kfzg4wwDQYJKoZIhvcNAQELBQAwgbQxCzAJBgNVBAYTAlVTMRAwDgYDVQQIEwdBcml6b25hMRMwEQYDVQQHEwpTY290dHNkYWxlMRowGAYDVQQKExFHb0RhZGR5LmNvbSwgSW5jLjEtMCsGA1UECxMkaHR0cDovL2NlcnRzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkvMTMwMQYDVQQDEypHbyBEYWRkeSBTZWN1cmUgQ2VydGlmaWNhdGUgQXV0aG9yaXR5IC0gRzIwHhcNMTYxMjA4MjMwMjAxWhcNMTcxMjA4MjMwMjAxWjA9MSEwHwYDVQQLExhEb21haW4gQ29udHJvbCBWYWxpZGF0ZWQxGDAWBgNVBAMMDyoudGVzdHRpdHVzLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALjEcBtpSTXWYMLu+AKVI5kpYp2uwUIyNpagfUOT6fhzRGjoO8UjCuvYqj+EW0rEiegzHoAPYQ7pzUxrCcFP6ePEAppA3wHUzUUBJH5bO3KpTNbNNlkNWiAQF5sowgB4xLjF2q/PX/kYMSGbx2OyF03i0gaJhzIrfMeP76DZwTo2kwFwq5GbHwMHXP81R7Zk2+rArUf6naWaPXy9Dkh+NBEVOPP1wddGPoSmBWVUwg5YM2hIwtuvMlT8M4I2BrZJJGWmJwU2zaoOkh9Yj+XznwW0IAGX7rsLZxC2q6hKvz8XiDb1l+VObtp+M1XKLXf812dS1nPgc6xdvfXU9RBtEAsCAwEAAaOCAbwwggG4MAwGA1UdEwEB/wQCMAAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMA4GA1UdDwEB/wQEAwIFoDA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmdvZGFkZHkuY29tL2dkaWcyczEtMzYwLmNybDBdBgNVHSAEVjBUMEgGC2CGSAGG/W0BBxcBMDkwNwYIKwYBBQUHAgEWK2h0dHA6Ly9jZXJ0aWZpY2F0ZXMuZ29kYWRkeS5jb20vcmVwb3NpdG9yeS8wCAYGZ4EMAQIBMHYGCCsGAQUFBwEBBGowaDAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZ29kYWRkeS5jb20vMEAGCCsGAQUFBzAChjRodHRwOi8vY2VydGlmaWNhdGVzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkvZ2RpZzIuY3J0MB8GA1UdIwQYMBaAFEDCvSeOzDSDMKIz1/tss/C0LIDOMCkGA1UdEQQiMCCCDyoudGVzdHRpdHVzLmNvbYINdGVzdHRpdHVzLmNvbTAdBgNVHQ4EFgQUlzrOazMLppE1vwQ3LrtIB66WYgkwDQYJKoZIhvcNAQELBQADggEBACNc29K07U5mCFbwIaedbFjY1WayDLDUrRiWgm6RiVC9xAJHkQOO/6e8wqRwCFKgIkH1R1kCFWOXVuVhynsF5A9G/txItxC5cc+UMqe0Dgs+B0nQu+9yOKdxmbyALB5Wf+gZ1I8YIDa2ugdq4SXCgkleSnP1INmLe0ZPwmuBZBCtF+i2X40E7HzDBoWliCCHEF7pdWvg5DeOg0GBeg9D1wBUdpF36x9HRePk0MR+W6RgZe1KTGhOunIAQJm2/H0sTU+9+9q7k2/JZvN5ATQF4ffOI97CK+w5/qVQU8PpSt9mQETUgaj5U10cfk45e0tNUYlu8d2MjMB+/gPFuY1DBz4=&lt;/X509Certificate&gt;&lt;/X509Data&gt;&lt;/KeyInfo&gt;&lt;/Signature&gt;&lt;/saml:Assertion&gt` });
  private customerApiUrl = `${this.baseUrl}/customers`;  // URL to web api

  constructor(http: Http) {
    super(http, "customers");
    //this.headers.append('Content-Type', 'application/json');
    //this.headers.append('Cookie', `ARRAffinity=851d7f30361f6d0723ca121b2bd6d6718ac17eb6e82712885034bbf6fcabc392; FedAuth=77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U2VjdXJpdHlDb250ZXh0VG9rZW4gcDE6SWQ9Il82ODY4ODc4OC1jN2U0LTRlMGYtYTJkOS1jZGUzZTdmYzVkYTQtNTNFM0UwOTdDNkJENjU2OEVBOUQ3Mzk0MTlGOEFCNzciIHhtbG5zOnAxPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktdXRpbGl0eS0xLjAueHNkIiB4bWxucz0iaHR0cDovL2RvY3Mub2FzaXMtb3Blbi5vcmcvd3Mtc3gvd3Mtc2VjdXJlY29udmVyc2F0aW9uLzIwMDUxMiI+PElkZW50aWZpZXI+dXJuOnV1aWQ6MWU5YTMyYzYtZjYzZS00ODgwLTk3MGMtMGE1OWQ0MzBhYmEzPC9JZGVudGlmaWVyPjxDb29raWUgeG1sbnM9Imh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwNi8wNS9zZWN1cml0eSI+Vm1wejhuMVBWcVhzSVVOWGthcFh5a1RiT2RrcXZvVWFRU09YSlUrMGRVemYxNndNa2g3eVh6cTFsZ3lvWm0rWHg2aWd5dlRaZk1BV1ZNdm9lU2VGUDB3dGZrRnhyL3BTRWZWSlBoRjVCRkRhVDlrWmptaW9Xb1MzWmZWQ0RLRmxlbkJOWlhBS0lxV1c0OU56T1owU1dqclBkK0EwZ1cxd0tWdzRJZWp5ZHJHRGdDdE5mTDBlT2xTdDU3NVdIemJudzV3Ulpua0VsWGt5ZzdhYnN1enVGaFBEc0tYcGVSRURKL280Qys0MHBpd0VmTTIyQmMrbDkzL2NVdmsreHBSZWdyeTNLS3JSdWtZaWhFbGdSQjVWN0pKM1BpRGJaNGxSRnV5VGExbmdyNzkwc3BQblhTMEJ0S1BTTUNxN2gxQzZWU3hLY2lOY1U3akcrQUo2dGJjdnB5MVRaa29hcnhxanEzWk44dFRuTmVKUXZCU1h0UzYwZTJmdWlGdTZrY1VOMmNna250RG1Yc1V0TitPVEJSL2ZZa2dOeHRRR1JjcWErK1ppUWpvY2tlaThNdGpDRFhmT0Y2ajJmbE9NQXNpZWhOTVJVYVJhSXlRVGlLQzhsODJ1NVJJNzNLTkVmTGEzUE41eGd4U3Q2dUo3QllkbG4zaVE5SkFMY3ljMmxiZG5IcWtpUGdZTVNqcFI1Mm1XNEhTV0NlajFIeU0rY09rRUJkWWFtQnJwM0Zkck05WmxYQUdaOXJ5ZEd6ZWtyRmtrSkIveGJEQ3hYZWluV1hsSXZkQThzaGdTZmJUN0tIWENSNnlmOVBjbVIzUDJrODhhSklaWmpxcVdnOHJteXRBS3VWNzNrTmp2QXlVVkhDUm9oU2RQQW5xb0svRThrOXVTNVo3aFBFY1ZzTGhKbnBWTVExMlY1ZGw1RVpzSjViUld4VkpMUGNKUlRzK0UzT2dmanlkTnl3ZnJqdmRJL2JmTkVPREwzSXVwZDFoalBmMW9YMkVRQk9sOVZRNW5pZ0I0VWhRamtKSnYxc3NXTHFaaXZjbmtOb0NqM3B6a2VONExnSTNHRlJueS9IV0taMUpMbmJBaXRuazVwSXBzNVNMdER1YU1OdGErK3hnbWVaRGp2OTZ2Y2J5ZGY2cU5UVURzaENMQ1loaENxSXh1VHVIaGZvLzJQVzZ6eUMzLzVnbnBmNXMzTnlCSzJjU2hhNHo1emFSa3AxSm1wSndCTmRDRVRrY3E3a0dJbzZQU2xENlZEaVE5MWlCQVBvUUpDbGJHY1FWNWNUNmtjS2NCbFh6Yjk2NG5Lejdsa3NrQ1BlVDcxSjZVZzF1dUkzNWVpR2JEUDZOT0VSRHhjL0g2anZUa0hROTVDaDVSUlliaThrZjdCSjBFN092WWNyVEJ4OS9LeDVa; FedAuth1=NXZMNnllL3RYeFIwTUlZbWxTaS9sb2pkZzFkRzlHRnNPZXp3c1FDZitPUERXdWFXWGI1SHhRNkxRanR3blhCY0w5aUxYODlLdmZQdGJxKzFkWlFKazU5dDExYktiTHZ6Y2gwQ3M5c0w2MnVPZmJHM21iSmJaeE1KcVVjTGQrc3U2RytnTTRBelI4Q2s5QnFEb3QzMmJoUm1xcTZUb01KZWhka0s2eHFLb1orYmNDSXlMdUllMmlwMEZnUXQwdzBBMHB1WGs3eTNJdkRNWUc1OEtKTnF3dHlFb1BXQkd1dnQ3UVZyK3Irckc2YWZ3UHE4Y3JiOUhWUHcxTDN6UUxXTmpUbjZYcVJkWFF1Z3Azd3dUSk9GM0ZEa1YzWGZWRGFldnd0ZlIrUVpHb2UreXFFRXIxZFZoelRwWnFkVzdJL3ovclFkSXFmZ1Bya1FOVUNMUkRqN0JnbEh3WEV3K0c4enpadUtHbmp5ODZlRjRmeFcvR1pOeWFBeTlTZGJQTnhyS0pGcUhkTk84U05OT0Y3YjZQSlpxeUJkMGJjMlJiK1FEMHVZSE5JU2h2TzJaeHRlVXB4Y2RxV0pRS2xTRUw0RWMvcUdpRDJjTXhOaTRoeFdWNy9EVkFVSFNsV3VEZDhsQU9XWDRPeC9YR2cvQ21pK2xoc2JWb2t3SHpyMmlPWm9XMjJIT0dpbS9Uck1yNjJrcE9NTUZNaE9QaTV0VitTMjlSTFBXSmFxekg1cHFRdTdqU2YxeHJqaUYrQ3RQcndQTFR2anhCb0JGaElUS0FHNERzZ0ZydmpNUU8yYmZWK25QTEoxa0dBVUM4M0c0UHZvTTV6dmVHRjYvaGZSazZHTDFDMDR1elVCZUl0cG9YTFRjeWUrSXVNL0gvNVZKMWdtRlU4c2FDRnhSeEo0OXlER3FwQkJUUTBFblhxVXhtOHpjVW5DakdPZUY3Mkp3c2pXaENOL3ZCYStWWFBXNXRsdWV1d0d5VFIxbUthcFlRZ1VZdkEwc1NkRTNPNmFoMXhSTWx3MC9VUjR6N0ZuTGpQZU9ieHRhaHFXbFoxVzFuUWFyRmZVYm43ZWN1UnA4ZmdvN1ZrN25CbkZ5UGcrMm05ZnJ2dHgxc1pTZ1ZBR1dBdWhyeW1MNWZWY09ORS9RTGdMR0c4V1pZNzExKzFjU3BMZEtCMVhNTDlvUUx5ekdMdGlFK21aQ2ZMK2NSR0tyM1UvdUFVNHFTVG5JWGFjOXFBZzNWWVVVTmpXakFBdGk0UDcyNHdKbjkyY2RPZ3BZVW9VTEF3K1lPWkV1NUlrVUZIMUpmV1V6TkxqLytUNUt2anZacFAweDFSY2JIZGdUSnVxbCtFR095Y3VLWWdsWDM2MnhoRVRsYmkvVklNTW42ZW52VnhieTJ1NkFPTWRIazU2djZ6ZlBYY1RVa05CamVBWUJCQkVsclg1bXFYeEVnOUZUWkJTTzMzQXB0OHI2ZmJCUFJUMm5KZjM1alpOODhlbCtNaHJCbnd2TzU2SitUSEZZQmZJVS84VlVSZlBxSytsR05FVm9jbVc2ZXMwdXR4OXlveUpXQ3BnS3kxNm1uWkFVTEJmQzFYZWlqY1l0c09jRXhvajN5aWxkS1ZHWkhUUnN1bElnOVZXWUVRV093dk9NM0JXdFpVQ2lhL0lFU29CbldVVVk3YkdTM2tSVGZFTmNuMUdrMEdFSlM0OG8rQ1o0cVp3bS9QWUx0MXdDTFEwREs3ZUpERHQvVVoreXVYek9GM1pOQ2Z4cXdyVDhGRVovbDFNdTVWR2pKb00xU0RjZ3RTWG5rVmdTRE9wS1IzMTR2akpJaDd2S1FRNnQvREVvZDNEbEhwelpmZ3dPZDFsSk9mamtXOFlaVXlJTndwejVTN1lsSXRzZW9YS0RXQVFHOTFaSTNXK01sZ00zakhlaVYvNThXZG0xUGlOR0FIUW90a3IwVUJHa2lRS2prU3JveDNTbm01NTJpdk15V0E3eWNKVkRYT2JzQ3BE; FedAuth2=Tm1NNjRJeW84QlIxa0hncFBudFdCeTRZRldpbGlJeFNwRGZLdUVwQmVJK2tiVFcyQ1NjbFlrVGtGbWdNTGJjYnZpa3VVZWxKckJSOWQwTFNqWmY0a0d1K2t3cTcyM3l3OXlHM3R5L2ZmdERENk54emJIOE1VSEVsWm9CQXN5SnJjUFhiQ3FOQXVqcEZaZDVMMEJHNC9taFl3MkgxSHd4cCtZSC9EMGszWVRkNm1vWTZ3QUFadGlNL1ZDR3ZwWU5rUVJnN3pad1VhZVdrU05pU2RzZkg5Qkw2OWcwTFBGMnUxdGx1Z2V5RGY2VDcramtiL0djUUFVZ3lXRThNUUdtSk8rdmdyK1pNMHhBbWtOVmxQdDMvQnJ6MXMxNHlmenlsQkFZQis4S1BGTjZvWUhyMW9yczNpMnhDWW5rYVcrMlZaT0VvSjVDSXIrZjhwRlZtcVRHNWdKMkQxMmlveHdvZko5ZDJ5WEZuRThlVFJRckZLNVRFUzN1KzR4WThadnAwYTgvOUYxVWV5UUZLUzJORnNJTDljaHN5cmhsRUFEU0plbmhoYkVPU2tmVWNURVA3T0V2VzR2MzFPa2hQU2pSQ0Fjd09oOTlUOFpKOXNIeWJ1cSsrWUh0UGFiSW9TTEZmaEhFeTBqdXJycEtrdHhiSnM3cHhWNVFtZ1VXYW4zZURZYnFFVjB4N3kyVEI4c2xGd25JeGxlRytXQ0twdFc2bVI0QlVnbDZPQ2w3R0JjTUJCcWJ4T0Y2cXFmMk9VZTBjbnFUNHlRMDFtZk5scXB1OEF2M1BiL3FjclRSNmkyYy9CaTdyZVlDT0d4RWZvY3MzbjRsUEdaN2JTczFqMjM0TjIvcTU5RDF3bXUzNEMzZnBEV3hmQ0JuRmVlOTVxVnc2Vk9jYU9VNGRhZk5ISmlvTVF4bE05dFQ4SmppSDlTRkNJTTRQbDFWeXI2YTUyd1pCbVNNUjhXMWMrcDdtQ2FDaFZZRkVUcWxDVEF3QzQyUGgrck5vTmN1NWdtVytOMXhEYjBiczl0NnloRDRrTFlkMGRtQ0FVK08yTWtoNWI5NDg2eWkxcEFKN1NBZkd6VEFVYnNTRXFFY2QrNlFZRzNlSGM3ZXdmZ3M0STV4OWpnVkxOTHpOYmNzMkliSEJhc05MVHlqVUNTQUg1ZS9Ec1Y0dE52VURMSStpN3RqcVByYzY2UnlmUnR4ZzQzaUl4dTJnOFdnTEkyZWR1YVdIc0hRZVNpQU5qajI2NS9xdXVZb1h6a0M2aHdhTWxWS285aGYrRFprb3NFU3kraXcxeFdyVUl3bGNnM0lEK1hKWmRyN1A1OExBQVdKdFY4UFBsR0c2QjNOYkRNdG9zUUxrTXNNOWFvcWh5am1nbVRtZTdydlJrdldTWG5mdWdETlRDbzZoTWJDNmZiV0R6TkNDeURSY2xsWWdtNmx5bVB2OEJxN1RJb0dTQ25VdlI2MDlFejRDYjFZQzU5Mzc3Y3JGb3lwTDJnUVh3R3gvdFRlR2huM0prcVRDeFc4TUtpMitXTDA5cDFneWZ3Q0ltYVl0MWJOem9EUDNtditCREJVY3BGbjIvVW4xQUh2L1JEaWhQUGNHUVkzY0g4eWJ5RGpwYzlLZFU3ZmozT2VTTjQ4cDFSMkFiVDlWeXlnUlNSRURaSWp2TEVvOFhZVE5UNkJmN0t4RkNGSTZKREEyTWk1SVZneWxxajd4WW5CcjJUK3Z3WTExMGQyU295a3JOSElyT3JqQm9OS3BnSGZldXZCOUt2RnNQUHppb0RtVklCMjVBalNXdkhQMWY3czVobjlITzN5Y2hWL1docXdvcmFmSzdkbU5QSG4zaDdLS0lBcGtVZ0FGQ0dmL0QxTkUzQ3pHRHlBODlvbnVGRzZiWHlucHZxTm5YazJUZDZycDJGcUpONjdFdWdLMXVjRGU1RWtxUElsSTh6a1dENGlaQkxsMHpVaUdKNWVKcWw0TDROdGl5UWpWSUluZks4TEpyS0hB; FedAuth3=SHE5Zm83TWtsZVE1V2FrUUVZQjJXV0l6RkhkYkFoUlM5N0MrSGZtUnNNc0huR3Fqa0ZTRUJBRi90b0pjYWFVNUJYZGpEZXhiRm9xekpSOVB4T0grdVBMOXB5ZEpzVmErTnNsd1c5bnhtYVBCMDYveFNac3pvMjFUQnEvRXQvSGRhRE5ic1p2MGhUOCsxYXBHNnVKTWlwdjBHRit5N1pSdFdtWlEySFNjYVg3VjMzdEZoK21JdFYyMnUvQ1N1SHZPVGFzby9zYnlWMmlkbnNDcWZISUZFTThmVkdYK2lweThQaGFyeVZyek5RZGJRbUVETjl6UjJ3eWI4OEdhNlU2RzJhNnVZNXg3MndrTDE3LzRCalVhb0F3bDBmYnB5clM0VzVOcThDcFlCNTJHeXhiUGxUVWd4WjN5cUFRLzhMNXRQcEc0THQzcW5JNTlmUGNrQTNkcGZuaWNTUHFrNk5BVzlwZ0xUTHl2S0RoV2ZrNTROZnVwVjAzMWZKUUlxcmpJbmJPcnM3UmZKcGZ1QTdtbWtKa0JJbklNbWVxWUpBYVBLSmpHU0ZNRWN2dTFDYmkxdXY2Vzc5MHlsWXVFaExmR0k4b2NlM2xzczYxWFFHT2FMNmNZRlJrU29jVkxBdWQxaGV2M01obEpvMEdaMlFHUXV0WkJjTlNiQ1JDaUV2T1VJc2Z5YmFTSXh5eGlQTThibi9Ec3ViWnVxeS81SkZVaWtQQytVRWpMa3FIbHI3ZVdRczFDR2k1eGM5cDBJQklkeS8vL1JQREMvVWhzSjJCaERhK1U1MDluU2ZydGxUamNGVkk2RVkwOWE2SHVxTnVsYWdBODdrcW8rc2VGb0ZNamllT1IwMFIxMlVlMXFtVjJsUXdHemJmOHhSc2ZrcERVMFlzS2hQb2VWbkN6dXh6aUJpQW5OcWxJSnJURVIzK2NOVkR6aXhUMmpzUEtrVnZhaXB1N0RIekdPMW1lTzRZNlJqNFJaNnFTU2RtWWtoYVVJTDdsT2lqd0pqbHNXaUM3S05kajk3SmdJQU1HWllqVzI0Z01PU3k2ZUlpUU10cThURkowTUNSaG0vQW9rcEhIdEMvRmlBL0FqazZXcStDM1F1Nk9DMmxqUUdDeXZ1aUZwbFNHa3ptUnhtNE9JSjNJVkZPZW1JU1o2NzlVdWxjQ2VyNFVVNjN3cFlPNDB1enpxeThONjBtT0p1ZFlaMU9QYW5HVW5odURBQ3lBTmsvNW40eis4blRsS0t6NEh6djZYdnYvS0RtVUpkcWRNYmE5RzBaK3M5QnpOMCtMNllZcFFhd3dPZjdvMTZOMXlUWU1FbG1oZG5RZWFRaDZYVmw3SUt4L2FPblVYVWY0MC9QeUpyeFErVTMrcmxMaC9seU1oRXJ6MHNUVVVMTHdmeTZTazBWd1A3ZWVaMHVsZmIvYWR4RmpvU2lmZlBpY2tJb3pzNG8yaXFEMlZJVmdXNGVvUmdJUGpndFRKMEVteHM2YytpZXVwQmRDTmNFZGpENE4xS0Rqa3RNN0JVekNCS1lDUUxQbmNCd1JXVlYxVEZwQk9ibktVZHFHUEZrV0h4NlVkZXZzSUZ3L1B5Z2JhUWlPdm14TG9DRkt3MDlvQURkWXlMSk5WMlY3Y2o5eFBXd0E3bWxSbkd6ZlFST0Nkdk5oaUFGeEJiUTZSM1VNSVplVnRNb3JrY0QrTVlDZmYvZmJ0TVRyeXdOOVlQNFNTcEdVTUEzZG92bUFRcDY1U21VYXFzRDJ4RjZTOEFvNVFHb0dwVFVBVTRCTUJKMUt3YmJkUDhNQXRSYzQvOGZEdmlvVXVTWmIrcXVWVFBlSTFWOEp4UkdlZjVlanoyRWFpNG9sRm1PUnRERGNYNENlWDBXMnZHZWIwd1Y0TUtWM25LeDQ9PC9Db29raWU+PC9TZWN1cml0eUNvbnRleHRUb2tlbj4=`);
  }

  getCustomers(): Observable<GridDataResult> {
    return this.http.get(`${this.customerApiUrl}`)
      .map(response => response.json())
      .map(response => {
        let res = response as any[];
        return (<GridDataResult>{
          data: res,
          total: res.length
        });
      });
  }

  getCustomer(id: string): Promise<any> {
    const url = `${this.customerApiUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as any)
      .catch(this.handleError);
  }

  getTenant(id: string): Promise<any> {
    const url = `${this.customerApiUrl}/tenant/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as any)
      .catch(this.handleError);
  }

  removeTenant(id: string): Promise<void> {
    const url = `${this.customerApiUrl}/${id}`;
    return this.http.delete(url, { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  updateTenant(tenant: Tenant): Promise<Tenant> {
    const url = `${this.customerApiUrl}/${tenant.id}`;
    return this.http
      .put(url, JSON.stringify(tenant), { headers: this.headers })
      .toPromise()
      .then(() => tenant)
      .catch(this.handleError);
  }

  //create(name: string): Promise<Customer> {
  //  return this.http
  //    .post(this.customerApiUrl, JSON.stringify({ name: name }), { headers: this.headers })
  //    .toPromise()
  //    .then(res => res.json().data as Customer)
  //    .catch(this.handleError);
  //}

  //update(customer: Customer): Promise<Customer> {
  //  const url = `${this.customerApiUrl}/${customer.id}`;
  //  return this.http
  //    .put(url, JSON.stringify(customer), { headers: this.headers })
  //    .toPromise()
  //    .then(() => customer)
  //    .catch(this.handleError);
  //}

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
