﻿export class Tenant {
  id: string;
  name: string;
  tenantid: string;
  syncfresh: boolean;
} 