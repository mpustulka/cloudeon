﻿import { Http } from '@angular/http';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';

export abstract class CMDBService extends BehaviorSubject<GridDataResult> {
  protected baseUrl: string = 'http://localhost:53666/api';'http://cmdb-api-data-test.azurewebsites.net/api';

  constructor(protected http: Http, private tableName: string) {
    super(null);
  }

  public fetch(tableName: string): Observable<GridDataResult> {
    return this.http
      .get(`${this.baseUrl}/${tableName}`)
      .map(response => response.json())
      .map(response => {
        let res = response as any[];
        return (<GridDataResult>{
          data: res,
          total: res.length
        });
      });
  }
}