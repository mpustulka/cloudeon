﻿import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { GridDataResult } from '@progress/kendo-angular-grid';

import 'rxjs/add/operator/toPromise';

import { CMDBService} from './cmdb.service';


@Injectable()
export class CIService extends CMDBService {

  private ciApiUrl = `${this.baseUrl}/ci`; // URL to web api

  constructor(http: Http) {
    super(http, "ci");
  }

  getCIExcelURL(): string {
    return this.ciApiUrl + `/loadcsv`;
  }

  getCIList(): Observable<GridDataResult> {
    return this.http.get(`${this.ciApiUrl}`)
      .map(response => response.json())
      .map(response => {
        let res = response as any[];
        return (<GridDataResult>{
          data: res,
          total: res.length
        });
      });
  }

  getDiscoveredCIList(): Observable<GridDataResult> {
    return this.http.get(`${this.ciApiUrl}/discovered`)
      .map(response => response.json())
      .map(response => {
        let res = response as any[];
        return (<GridDataResult>{
          data: res,
          total: res.length
        });
      });
  }


  getDeclinedCIList(): Observable<GridDataResult> {
    return this.http.get(`${this.ciApiUrl}/declined`)
      .map(response => response.json())
      .map(response => {
        let res = response as any[];
        return (<GridDataResult>{
          data: res,
          total: res.length
        });
      });
  }

  getCITypesList(): Observable<GridDataResult> {
    return this.http.get(`${this.ciApiUrl}/types`)
      .map(response => response.json())
      .map(response => {
        let res = response as any[];
        return (<GridDataResult>{
          data: res,
          total: res.length
        });
      });
  }

  getCI(id: string): Promise<any> {
    const url = `${this.ciApiUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as any)
      .catch(this.handleError);
  }

  changeCIState(id: string, state: boolean): void {
    this.http.put(`${this.ciApiUrl}/changecistate/${id}/${state}`,null).catch(this.handleError).subscribe();
  }

  create(ci: any): Promise<any> {
    return this.http.put(`${this.ciApiUrl}/create`, {value: JSON.stringify(ci)}).toPromise().then(response => response.json() as any).catch(this.handleError);
  }

  bulkChangeCIState(data: any[]): void {
    let headers = new Headers({ 'Content-Type': 'application/json; charset=UTF-8' });
    headers.append('Accept','application/json');
    this.http.post(`${this.ciApiUrl}/bulkchangecistate`, { value: data}, new RequestOptions({ headers: headers })).catch(this.handleError).subscribe();
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
