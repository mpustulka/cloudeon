﻿$(document).ready(function(){
    $moving_tab = $('<div class="moving-tab"/>');
    $('.sidebar .nav-container').append($moving_tab);

    $this = $('.sidebar .nav').find('li.active a');
    animationSidebar($this, false);
    $('div').removeClass('.moving-tab');
    if (window.history && window.history.pushState) {
        $(window).on('popstate', function() {
            setTimeout(function(){
                $this = $('.sidebar .nav').find('li.active a');
                animationSidebar($this,true);
            },1);

        });

    }
});
$(window).resize(function(){
    $this = $('.sidebar .nav').find('li.active a');
    animationSidebar($this,true);

});
$('.sidebar .nav > li > a').click(function(){
    $this = $(this);
    animationSidebar($this, true);
});

function animationSidebar($this, animate) {
  if ($this == null || $this == undefined || $this.length == 0) return;
    $current_li_distance = $this.parent('li').position().top - 10;

    button_text = $this.html();

    $(".moving-tab").css("width", 230 + "px");

    if(animate){
        $('.moving-tab').css({
            'transform':'translate3d(0,' + $current_li_distance + 'px, 0)',
            'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
        });
    }else{
        $('.moving-tab').css({
            'transform':'translate3d(0,' + $current_li_distance + 'px, 0)'
        });
    }

    setTimeout(function(){
        $('.moving-tab').html(button_text);
    }, 100);
}
