﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Repository;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Documents;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAllOrigins")]
    public class CustomersController : Controller
    {
        public async Task<IEnumerable<Document>> Get()
        {
            return await DocumentDBRepository.GetCustomersAsync();
        }

        [HttpGet("tenant/{id:guid}")]
        public async Task<IEnumerable<Document>> GetTenants(string id)
        {
            return await DocumentDBRepository.GetTenantsAsync(id);
        }

        // GET api/values/5
        [HttpGet("{id:guid}")]
        public async Task<Document> Get(string id)
        {
            return await DocumentDBRepository.GetDocumentByIdAsync(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
