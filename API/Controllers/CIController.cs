﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Repository;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAllOrigins")]
    public class CIController : Controller
    {
        public async Task<IEnumerable<Document>> Get()
        {
            return await DocumentDBRepository.GetCIListAsync();
        }

        [HttpGet("discovered")]
        public async Task<IEnumerable<Document>> GetDiscovered()
        {
            return await DocumentDBRepository.GetDiscoveredCIListAsync();
        }

        [HttpGet("declined")]
        public async Task<IEnumerable<Document>> GetDeclined()
        {
            return await DocumentDBRepository.GetDeclinedCIListAsync();
        }

        [HttpGet("types")]
        public async Task<IEnumerable<string>> GetCITypes()
        {
            return await DocumentDBRepository.GetCITypesAsync();
        }

        [HttpGet("{id:guid}")]
        public async Task<Document> Get(string id)
        {
            return await DocumentDBRepository.GetDocumentByIdAsync(id);
        }

        [HttpPost("loadcsv")]
        public async Task<ActionResult> LoadCSVFile(IFormFileCollection files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    using (var fileStream = file.OpenReadStream())
                    using (var ms = new MemoryStream())
                    using (var sr = new StreamReader(ms))
                    {
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        var csv = new CsvReader(sr, new CsvConfiguration(){Delimiter = ";"});
                        while (csv.Read())
                        {
                            var doc = new Document();
                            doc.SetPropertyValue("id", Guid.NewGuid());
                            doc.SetPropertyValue("discovered", true);
                            doc.SetPropertyValue("_syncfresh", null);
                            doc.SetPropertyValue("_created", DateTime.UtcNow);
                            for (int i = 0; i < csv.FieldHeaders.Length; i++)
                            {
                                doc.SetPropertyValue(csv.FieldHeaders[i].ToLowerInvariant(), csv.GetField<string>(i));
                            }
                            await DocumentDBRepository.CreateItemAsync(doc);
                        }                        
                    }
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        [HttpPut("changecistate/{id}/{state}")]
        public async Task<bool> ChangeCIState(string id, bool state)
        {
            return await DocumentDBRepository.ChangeCIStateAsync(id, state);
        }

        [HttpPost("bulkchangecistate")]
        public async Task<bool> BulkChangeCIState([FromBody]dynamic value)
        {
            try
            {
                var bulk = JObject.Parse(value.ToString());//JsonConvert.DeserializeObject(value.ToString());
                var list = (JArray)bulk["value"];
                foreach (var ci in list)
                {
                    await DocumentDBRepository.ChangeCIStateAsync(ci["id"].Value<string>(), ci["state"].Value<bool>());
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
                throw;
            }
        }

        [HttpPut("create")]
        public async Task<Document> CreateCI([FromBody]JObject value)
        {
            var doc = new Document();
            var obj = JObject.Parse(value.Value<string>("value"));
            
            foreach (var prop in obj.Properties())
            {
                Debug.WriteLine($"{prop.Name} - {prop.Value}");
                doc.SetPropertyValue(prop.Name, prop.Value);
            }
            
            return await DocumentDBRepository.CreateItemAsync(doc);
        }

    }
}
