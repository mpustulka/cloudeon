﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;

namespace API.Repository
{
    public static class DocumentDBRepository
    {
        private static readonly string DatabaseId = "cmdb";
        private static readonly string CollectionId = "entities";
        private static readonly string Endpoint = "https://cmdbdocdbprod.documents.azure.com:443/";
        private static readonly string AuthKey = "YjFKxJlIX8L2pycT1RlMRPLs1ahvo6xr7rUKk95KIvCIkbBsATjrFp72tiKp9RmOdiTK5x9bo9hqhnlOpN9pLw==";
        private static readonly DocumentClient Client = new DocumentClient(new Uri(Endpoint), AuthKey);

        //public static async Task<List<DocumentCollection>> GetCollectionsAsync()
        //{
        //    Database database = Client.CreateDatabaseQuery($"SELECT * FROM d WHERE d.id = \"{DatabaseId}\"").AsEnumerable().First();
        //    List<DocumentCollection> collections = Client.CreateDocumentCollectionQuery(database.SelfLink).ToList();

        //    return collections;
        //}

        private static async Task<IEnumerable<Document>> GetDocumentsAsync(string queryExpression)
        {
            IDocumentQuery<Document> query = Client
                .CreateDocumentQuery<Document>(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId),
                    queryExpression,
                    new FeedOptions { EnableCrossPartitionQuery = true, MaxItemCount = -1 }).AsDocumentQuery();
            List<Document> results = new List<Document>();
            while (query.HasMoreResults)
            {
                results.AddRange(await query.ExecuteNextAsync<Document>());
            }

            return results;
        }

        public static async Task<IEnumerable<Document>> GetCustomersAsync()
        {
            return await GetDocumentsAsync(
                "select c.id, c.name, IS_DEFINED(c.knownazuretypes) as isEditable from c where c._type = \"customer\"");
        }

        public static async Task<IEnumerable<Document>> GetCIListAsync()
        {
            return await GetDocumentsAsync(
                "select c.id, c.customername, c._created, c.type, c.resourcegroupname, c.name, c._lifecycle, c.subscriptionid, c._syncfresh from c where c._syncfresh = true order by c._created desc");
        }

        public static async Task<IEnumerable<Document>> GetDiscoveredCIListAsync()
        {
            return await GetDocumentsAsync("select c.id, c.customername, c._created, c.type, c.resourcegroupname, c.name from c where c._syncfresh = null or (NOT is_defined(c._syncfresh) and c.discovered = true)");
        }

        public static async Task<IEnumerable<Document>> GetDeclinedCIListAsync()
        {
            return await GetDocumentsAsync(
                "select c.id, c.customername, c._created, c.type, c.resourcegroupname, c.name, c._syncfreshdate, c._syncfreshrejectedby, c._syncfreshreason from c where c._syncfresh = false order by c._created desc");
        }

        public static async Task<IEnumerable<string>> GetCITypesAsync()
        {
            var result = await GetDocumentsAsync(
                "select c.type from c where c._syncfresh = true");
            var types = new HashSet<string>();
            foreach (var document in result)
            {
                types.Add(document.GetPropertyValue<string>("type"));
            }
            return types;
        }

        public static async Task<Document> GetDocumentByIdAsync(string id)
        {
            try
            {
                Document document = await Client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
                return document;
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }
                throw;
            }
        }

        public static async Task<IEnumerable<Document>> GetTenantsAsync(string id)
        {
            return await GetDocumentsAsync(
                $"select c.id, c.name, c.tenantid, c._parent, c._type, c._syncfresh from c where c._type = \"AzureTenant\" and c._parent = \"{id}\"");
        }

        public static async Task<Document> CreateItemAsync(object item)
        {
            return await Client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId), item);
        }

        public static async Task<Document> UpdateItemAsync(string id, object item)
        {
            return await Client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id), item);
        }

        public static async Task<bool> ChangeCIStateAsync(string id, bool state)
        {
            try
            {
                Document document = await Client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
                document.SetPropertyValue("_syncfresh", state);
                await Client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id), document);
                return true;
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return false;
                }
                throw;
            }

        }
    }
}